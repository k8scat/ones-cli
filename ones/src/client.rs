pub struct Client {
    email: String,
    password: String,
}

impl Client {
    pub fn new(email: String, password: String) -> Client {
        Client { email, password }
    }

    pub fn print_email(&self) {
        println!("email: {}, password: {}", self.email, self.password)
    }
}
